"""
Read support for larva_dataset_*.hdf5 interim files from MaggotUBA.

This module exists for testing purposes and the file format should not be considered as
fully supported. The module provides the core logic for `Formats.MaggotUBA`.

To visualize track segments in LarvaTagger, first extract segments originating from a
particular tracking data file using `extract_track_segments` with `allow_overlap=false`.
"""
module MaggotUBA

using ..LarvaBase
using ..Datasets
using HDF5
using OrderedCollections

export read_larva_dataset_hdf5, extract_track_segments

function read_larva_dataset_hdf5(path)
    h5open(path, "r") do h5
        labels = read(h5, "labels")
        samples = h5["samples"]
        nsamples = read_attribute(samples, "n_samples")
        tracks = Track[]
        for sampleid in 0:nsamples-1
            sample = samples["sample_$sampleid"]
            label = read_attribute(sample, "behavior")
            @assert label in labels
            data = read(sample)
            data = permutedims(data, reverse(1:ndims(data)))
            t = data[:,1]
            spines = data[:,9:end]
            spines = [convert(Spine, Path(row)) for row in eachrow(spines)]
            track = Track(sampleid, t, Dict(
                :labels => fill(label, length(t)),
                :spine => spines,
            ))
            push!(tracks, track)
        end
        parts = split(basename(path), '_')
        runid = join(parts[3:5])
        attr = Dict(
            :metadata => Dict(:id => runid, :filename => basename(path)),
            :labels => labels,
        )
        Run(runid, attr, tracks)
    end
end

function extract_track_segments(outputpath, inputpath, originalfile, tmin=0, tmax=Inf,
    allow_overlap=true)
    h5open(inputpath, "r") do h5
        labels = read(h5, "labels")
        labelcounts = Dict(label=>0 for label in labels)
        samples = h5["samples"]
        nsamples = read_attribute(samples, "n_samples")
        #
        segments = Tuple{Int, Float64}[]
        for sampleid in 0:nsamples-1
            sample = samples["sample_$sampleid"]
            file = read_attribute(sample, "path")
            if file == originalfile
                reftime = read_attribute(sample, "reference_time")
                if tmin <= reftime <= tmax
                    push!(segments, (sampleid, reftime))
                end
            end
        end
        sampleids = [id for (id, _) in sort(segments; by=last)]
        #
        h5open(outputpath, "w") do h5′
            g = create_group(h5′, "samples")
            t = 0
            extractedsamples = 0
            for sampleid in sampleids
                sample = samples["sample_$sampleid"]
                if !allow_overlap
                    data = read(sample)
                    data = permutedims(data, reverse(1:ndims(data)))
                    t <= data[1,1] || continue
                    t = data[end,1]
                end
                name = "sample_$extractedsamples"
                @info "Copying sample_$sampleid as" name
                copy_object(sample, g, name)
                labelcounts[read_attribute(g[name], "behavior")] += 1
                extractedsamples += 1
            end
            for attr in ("len_traj", "len_pred", "frame_interval")
                attributes(g)[attr] = read_attribute(samples, attr)
            end
            attributes(g)["n_samples"] = extractedsamples
            copy_object(h5, "labels", h5′, "labels")
            h5′["label_counts"] = [labelcounts[label] for label in labels]
        end
        return labelcounts
    end
end

end
