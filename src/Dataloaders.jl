"""
The Dataloaders module is a generic sampler of time segments for tracking or tagging data
files.

A dataloader is supported by a window construct for sampling and selecting time segments,
and a time segment index that can store pointers to all time segments in a database, or
emulate some global knowledge for databases that are too large to be fully indexed.

The default design is optimized for tracking data stored as json .label files.
"""
module Dataloaders

using ..LarvaBase: LarvaBase, times
using ..Datasets: Run, Track, TrackID
using ..Formats
using ..MWT: interpolate
using Random

export DataLoader, Repository, TimeWindow, ratiobasedsampling, buildindex, sample,
       extendedlength, prioritylabel, capacitysampling, samplesize!

"""
The default window type defines a time segment as the time of a segment-specific reference
point called anchor point. This anchor point is a defined time point (that does not need to
be interpolated). The associated segment is additionally characterized by its past and future
extent from the anchor point.

A window is in charge of interpolating or downsampling the time series data within a segment.
A time segment may also provide discrete behavioral information, typically sourced at the
anchor time only.

Interpolation/downsampling is automatically implemented for any datatype that features a
`samplerate` attribute.

A few assumptions and design choices are made:
* a window has access to the entire track hence the `indicator` function called to identify
  anchor points and related data (discrete behavior);
* discrete behavioral information (labels/tags) are provided as `Datasets.Track` data
  structures.
"""
struct TimeWindow
    durationbefore
    durationafter
    marginbefore
    marginafter
    samplerate
end

TimeWindow(duration) = TimeWindow(duration, nothing)
function TimeWindow(duration, samplerate; maggotuba_compatibility=false)
    0 < duration || throw("Non-positive time window duration")
    isnothing(samplerate) || 0 < samplerate || throw("Non-positive sampling frequency")
    margin = maggotuba_compatibility ? duration : 0
    TimeWindow(.5duration, .5duration, margin, margin, samplerate)
end

Base.length(w::TimeWindow) = round(Int, (w.durationbefore + w.durationafter) * w.samplerate) + 1

function extendedlength(w::TimeWindow)
    extendedduration = w.durationbefore + w.marginbefore + w.durationafter + w.marginafter
    round(Int, extendedduration * w.samplerate) + 1
end

"""
    TimeSegment(trackid, anchortime, window, class, timeseries)

Time segment representation that includes:
* the ID of the originating track (attribute `trackid`),
* the anchor time (*i.e.* the central timestamp, attribute `anchortime`),
* the time window used to generate the time segment (attribute `window`),
* the behavior label or label class (attribute `class`),
* and the timeseries data (attribute `timeseries`).
"""
struct TimeSegment
    trackid
    anchortime
    window
    class
    timeseries
end

Base.length(s::TimeSegment) = length(s.window)
extendedlength(s::TimeSegment) = extendedlength(s.window)
Base.minimum(s::TimeSegment) = round(s.anchortime - s.window.durationbefore; digits=4)
Base.maximum(s::TimeSegment) = round(s.anchortime + s.window.durationafter; digits=4)
extendedmin(s::TimeSegment) = round(s.anchortime - s.window.durationbefore - s.window.marginbefore; digits=4)
extendedmax(s::TimeSegment) = round(s.anchortime + s.window.durationafter + s.window.marginafter; digits=4)

LarvaBase.times(s::TimeSegment) = [round(t; digits=4) for t in range(minimum(s), maximum(s); length=length(s))]
extendedtimes(s::TimeSegment) = [round(t; digits=4) for t in range(extendedmin(s), extendedmax(s); length=extendedlength(s))]
LarvaBase.times(w::TimeWindow, t) = times(TimeSegment(nothing, t, w, nothing, nothing))
extendedtimes(w::TimeWindow, t) = extendedtimes(TimeSegment(nothing, t, w, nothing, nothing))

function indicator(window, track)
    ts = times(track)
    t0, t1 = first(ts), last(ts)
    t0′= round(t0 + window.durationbefore + window.marginbefore; digits=4)
    t1′= round(t1 - window.durationafter - window.marginafter; digits=4)
    range(searchsortedfirst(ts, t0′), searchsortedlast(ts, t1′);
          step=1) # explicit step arg is required by julia 1.6
end

function indicator(window::TimeWindow, segment::TimeSegment)
    @assert window === segment.window
    m = round(Int, window.marginbefore * window.samplerate)
    n = length(window)
    range(m + 1, m + n; step=1)
end

function segment(file, window, trackid, step, class)
    track = Formats.getnativerepr(file)[trackid]
    ts = times(track)
    @assert 1 < step < length(ts)
    @inbounds anchortime = round(ts[step]; digits=4)
    segmentdata = if isnothing(window.samplerate)
        @assert track isa LarvaBase.TimeSeries "Not implemented; try defining samplerate"
        t0, t1 = anchortime - window.durationbefore, anchortime + window.durationafter
        [(t, x) for (t, x) in track if t0 <= t <= t1]
    else
        window.durationafter == window.durationbefore || throw("Not implemented: asymmetric window")
        file′= isa(file, Formats.JSONLabels) ? file.dependencies[1] : file
        timeseries = gettimeseries(file′)[trackid]
        ts, xs = [t for (t, _) in timeseries], [x for (_, x) in timeseries]
        interpolate(ts, xs, extendedtimes(window, anchortime))
    end
    isnothing(window.samplerate) || @assert length(segmentdata) == extendedlength(window)
    TimeSegment(trackid, anchortime, window, class, segmentdata)
end

"""
    Repository(rootpath)
    Repository(rootpath, pattern; basename_only=false)
    Repository(rootpath, fileselector)

Data repository. Data file provider.

Files are recursively sought for from root directory `rootpath`.

Regular expression `pattern` can be used to control what files are included based on their
relative path or filename (if `basename_only=true`).

Instead, as a more general control mechanism, boolean function `fileselector` takes a
[`PreloadedFile`](@ref Main.PlanarLarvae.Formats.PreloadedFile) as input argument.
See also [`labelledfiles`](@ref Main.PlanarLarvae.Formats.labelledfiles), argument
`selection_rule`.
"""
struct Repository
    root::String
    files::Vector{Formats.PreloadedFile}
end

function Repository(root::String, pattern::Regex; basename_only::Bool=false)
    root = expanduser(root)
    files = Formats.PreloadedFile[]
    for (dir, _, files′) in walkdir(root; follow_symlinks=true)
        for file′ in files′
            file = joinpath(dir, file′)
            if !isnothing(Base.match(pattern, basename_only ? file′ : file))
                file″= try
                    preload(file; shallow=true)
                catch
                    @warn "Cannot preload file" file
                    continue
                end
                push!(files, file″)
            end
        end
    end
    Repository(root, files)
end

function Repository(root::String, fileselector::Function)
    Repository(root, labelledfiles(root; selection_rule=fileselector))
end

function Repository(root)
    if '*' in root
        parts = splitpath(root)
        i = findfirst(f -> '*' in f, parts)
        root = joinpath(parts[1:i-1]...) # splatting is required by julia 1.6
        pattern = joinpath(parts[i:end]...)
        if startswith(pattern, "**.")
            Repository(root, r".*[.]" * Regex(pattern[4:end]); basename_only=true)
        elseif startswith(pattern, "**/") # what about Windows paths?
            Repository(root, Regex(pattern[4:end]); basename_only=true)
        else
            Repository(root, Regex(pattern))
        end
    else
        Repository(root, collect(labelledfiles(root)))
    end
end

Base.isempty(repo::Repository) = isempty(repo.files)
Base.length(repo::Repository) = length(repo.files)
Base.getindex(repo::Repository, i) = repo.files[i]

root(repo::Repository) = repo.root
files(repo::Repository) = repo.files
filepaths(repo) = [relpath(file.source, root(repo)) for file in files(repo)]

"""
    DataLoader(repository, window, index)

Data loader as a simple combination of a datafile repository, a time window and an index of
time segments.
"""
struct DataLoader
    repository
    window
    index
end

root(loader::DataLoader) = root(loader.repository)
files(loader::DataLoader) = files(loader.repository)

function countlabels(loader::DataLoader; kwargs...)
    countlabels(loader.repository, loader.window; kwargs...)
end

const Count = Dict{Union{String, Vector{String}}, Int}

function countlabels(repository::Repository, window; unload=false, skiperrors=false)
    counts = Dict{Formats.PreloadedFile, Count}()
    ch = Channel() do ch
        foreach(files(repository)) do file
            put!(ch, file)
        end
    end
    c = Threads.Condition()
    Threads.foreach(ch) do file
        data = try
            Formats.getnativerepr(file)
        catch
            @error "Failed to load file" file=file.source
            if skiperrors
                return
            else
                rethrow()
            end
        end
        counts′= try
            countlabels(data, window)
        catch
            # "missing :tags field in NamedTuple" results from `drop_record!` in `sample`
            @error "Failed to load labels; are you reusing a DataLoader?" file=file.source
            rethrow()
        end
        unload && unload!(file; gc=true)
        lock(c)
        try
            counts[file] = counts′
        finally
            unlock(c)
        end
    end
    return counts
end

function countlabels(run::Run, window)
    counts = Count()
    for track in values(run)
        labelsequence = track[:labels][indicator(window, track)]
        for label in labelsequence
            counts[label] = get(counts, label, 0) + 1
        end
    end
    return counts
end

function countlabels(timeseries::LarvaBase.Larvae, window)
    counts = Count()
    for track in values(timeseries)
        foreach(track[indicator(window, track)]) do step
            _, state = step
            label = convert(Vector{String}, state[:tags])
            counts[label] = get(counts, label, 0) + 1
        end
    end
    return counts
end

function total(counts)
    iterator = values(counts)
    counts′, state = iterate(iterator)
    counts′= copy(counts′)
    next = iterate(iterator, state)
    while !isnothing(next)
        counts″, state = next
        for (label, count) in pairs(counts″)
            counts′[label] = get(counts′, label, 0) + count
        end
        next = iterate(iterator, state)
    end
    return counts′
end

total(counts::Dict{<:Any, Int}) = counts

"""
    LazyIndex(sampler)
    LazyIndex(maxcounts, targetcounts, sampler)

Index of time segments.

After calling [`buildindex`](@ref), attributes `maxcounts` and `targetcounts` are defined.

See also [`ratiobasedsampling`](@ref) and [`capacitysampling`](@ref).
"""
mutable struct LazyIndex
    maxcounts
    targetcounts
    sampler
end

LazyIndex(sampler) = LazyIndex(nothing, nothing, sampler)

function Base.length(ix::LazyIndex)
    isnothing(ix.targetcounts) && throw("index has not been built yet")
    sum(Iterators.flatten(values.(values(ix.targetcounts))))
end

abstract type RatioBasedSampling end

struct ClassRatios <: RatioBasedSampling
    selectors
    majority_minority_ratio
    rng
end

struct IntraClassRatios <: RatioBasedSampling
    selectors
    majority_minority_ratio
    intraclass
    rng
end

function withselectors(sampler::T, selectors) where {T}
    T((field === :selectors ? selectors : getfield(sampler, field) for field in fieldnames(T))...)
end

"""
    ratiobasedsampling(selectors, majority_minority_ratio; seed=nothing, rng=nothing)
    ratiobasedsampling(selectors, majority_minority_ratio, intraclass; ...)

Important! Returns a [`LazyIndex`](@ref) object.

Sample the labels considering two categories of classes: majority classes and minority
classes.
Minority classes share a same sample size (per class), aligned with the least represented
class.
Majority classes share another sample size, per default twice as large as that of minority
classes.
Argument `majority_minority_ratio` defines the sample size ratio between majority and
minority classes.

Classes are defined following arguments `selectors`. Per default, each label represents a
class.
When some observations come with multiple labels, one of these labels can be specified with
argument `intraclass` so that the observations with this label are systematically included.
"""
function ratiobasedsampling(selectors, majority_minority_ratio; seed=nothing, rng=nothing)
    if !isnothing(seed)
        if isnothing(rng)
            rng = Random.default_rng()
        end
        Random.seed!(rng, seed)
    end
    LazyIndex(ClassRatios(asselectors(selectors), majority_minority_ratio, rng))
end

function ratiobasedsampling(selectors, majority_minority_ratio, intraclass;
    seed=nothing, rng=nothing,
)
    if !isnothing(seed)
        if isnothing(rng)
            rng = Random.default_rng()
        end
        Random.seed!(rng, seed)
    end
    intraclass = if intraclass isa Pair
        Dict(asselector(intraclass.first) => intraclass.second)
    else
        Dict(asselector(selector) => f for (selector, f) in intraclass)
    end
    LazyIndex(IntraClassRatios(asselectors(selectors), majority_minority_ratio, intraclass,
                               rng))
end

function init!(_)
    @warn "init! is deprecated; calls to init! can safely be removed"
end

classtype(sampler::RatioBasedSampling) = classtype(sampler.selectors)
classtype(::AbstractDict{T, <:Any}) where {T} = T

abstract type LabelSelector end

struct PlainLabel{T} <: LabelSelector
    label::T
    complement::Bool
end

function PlainLabel(label::String)
    if label[1] == '¬'
        PlainLabel(label[nextind(label, 1):end], true)
    else
        PlainLabel(label, false)
    end
end

struct LabelPattern <: LabelSelector
    pattern::Regex
end

function match end # hides Base.match

match(sel::PlainLabel{T}, label::T) where {T} = (sel.complement ? (≠) : (==))(sel.label, label)
match(sel::PlainLabel{T}, labels::AbstractVector{T}) where {T} = (sel.complement ? (∉) : (∈))(sel.label, labels)

match(sel::LabelPattern, label::String) = !isnothing(Base.match(sel.pattern, label))
match(sel::LabelPattern, labels::AbstractVector{String}) = !all(label -> isnothing(Base.match(sel.pattern, label)), labels)

match(sel::Pair{Symbol, <:LabelSelector}, label) = match(sel.second, label)

function match(sel::LabelSelector, labels)
    for label in labels
        if match(sel, label)
            return label
        end
    end
end

function match(selectors, label)
    for (key, selector) in pairs(asselectors(selectors))
        if match(selector, label)
            return key
        end
    end
end

function indicator(selector::LabelSelector, labelsequence::AbstractVector)
    [i for (i, label) in enumerate(labelsequence) if !isnothing(match(selector, label))]
end
indicator(selector::LabelSelector, track::Track) = indicator(selector, track[:labels])

asselectors(undefined::Nothing) = undefined
asselectors(selectors::AbstractDict{Symbol, <:LabelSelector}) = selectors
asselectors(label::Union{String, Symbol}) = Dict(asselector(label))
asselectors(labels::AbstractVector{<:Union{String, Symbol}}) = Dict(asselector(label) for label in labels)
asselectors(regexes::AbstractDict{Symbol, Regex}) = Dict(asselector(regex) for regex in pairs(regexes))

asselector(selector::Pair{Symbol, <:LabelSelector}) = selector
asselector(label::String) = Symbol(label) => PlainLabel(label)
asselector(label::Symbol) = label => PlainLabel(string(label))
asselector(regex::Pair{Symbol, Regex}) = regex.first => LabelPattern(regex.second)

asselectors(sampler::RatioBasedSampling) = sampler.selectors
asselectors(index::LazyIndex) = asselectors(index.sampler)

function groupby(selectors, labelcounts)
    selectors = asselectors(selectors)
    totalcounts = total(labelcounts) # may already be done
    T = eltype(keys(selectors))
    T′= Set{eltype(keys(totalcounts))}
    classcounts = Dict{T, Int}()
    classes = Dict{T, T′}()
    for (label, count) in totalcounts
        class = match(selectors, label)
        if !isnothing(class)
            classcounts[class] = get(classcounts, class, 0) + count
            push!(get!(classes, class, T′()), label)
        end
    end
    return classcounts, classes
end

const TRXMAT_ACTION_LABELS = ["back", "cast", "hunch", "roll", "run", "stop"]
const TRXMAT_ACTION_MODIFIERS = Dict(:strong=>r"_strong$", :weak=>r"_weak$")

function countthresholds(counts, selectors, majority_minority_ratio)
    totalcounts = total(counts)
    classcounts, classes = groupby(selectors, totalcounts)
    isempty(classcounts) && @error "Not any specified label found" selectors totalcounts
    mincount, maxcount = countthresholds(classcounts, majority_minority_ratio)
    return mincount, maxcount, classcounts, classes
end

function countthresholds(counts, majority_minority_ratio)
    minoritylabel, mincount = first(counts)
    for (label, count) in pairs(counts)
        if count == 0
            @warn "Label not found" label
        elseif count < mincount
            minoritylabel, mincount = label, count
        end
    end
    maxcount = round(Int, majority_minority_ratio * mincount)
    return mincount, maxcount
end

function countthresholds(sampler::RatioBasedSampling, counts)
    countthresholds(counts, sampler.selectors, sampler.majority_minority_ratio)
end

"""
    buildindex(loader; unload=false, verbose=true, skiperrors=false)
    buildindex(index, repository, window; ...)

Build an index of time segments in a data repository, organized per label class.

`unload=true` frees memory early; timeseries data are unallocated as soon as consumed.

`verbose=true` prints a summary of time segment counts per label class.

`skiperrors=true` skips data files (typically *trx.mat* files) that cannot be loaded.
"""
function buildindex(loader::DataLoader; kwargs...)
    buildindex(loader.index, loader.repository, loader.window; kwargs...)
end

function buildindex(ix::LazyIndex, repository, window;
    unload=false, verbose=true, skiperrors=false,
)
    sampler = ix.sampler
    if hasproperty(sampler, :selectors) && isnothing(sampler.selectors)
        anyfile = first(files(repository))
        labels = getprimarylabels(anyfile)
        if verbose && !(anyfile isa Formats.JSONLabels)
            @info "Assuming any data file specifies all the labels" labels
        end
        ix.sampler = sampler = withselectors(sampler, asselectors(labels))
    end
    #
    ix.maxcounts = countlabels(repository, window; unload=unload, skiperrors=skiperrors)
    ix.targetcounts = buildindex(sampler, ix)
    #
    if verbose
        maxcounts = total(ix.maxcounts)
        targetcounts = total(ix.targetcounts)
        table = [((label isa Vector ? Symbol(Symbol.(label)) : Symbol(label))
                  => (count => get(targetcounts, label, 0)))
                 for (label, count) in pairs(maxcounts)]
        @info "Sample sizes (observed => selected):" table...
    end
end

function buildindex(sampler::RatioBasedSampling, ix)
    counts = ix.maxcounts
    globalratios = ratio(sampler, total(counts))
    targetcounts = empty(counts)
    for (file, counts′) in pairs(counts)
        targetcounts[file] = targetcounts′= empty(counts′)
        for (label, count) in pairs(counts′)
            targetcounts′[label] = if label in keys(globalratios)
                round(Int, count * globalratios[label])
            else
                0
            end
        end
    end
    @debug "Target ratios and counts" ratios=globalratios counts=targetcounts
    return targetcounts
end

function ratio(counts::AbstractDict{T, Int}; lower::Int=0, upper::Int=typemax(Int),
    ) where {T}
    mincount, maxcount = lower, upper
    ratios = Dict{T, Rational{Int}}()
    for (class, count) in pairs(counts)
        targetcount = maxcount < count ? maxcount : count < mincount ? 0 : count
        ratios[class] = targetcount // count
    end
    return ratios
end

function ungroupby(::Type{T′}, classes, classratios) where {T′}
    T = eltype(first(values(classes)))
    ratios = Dict{T, T′}()
    for (class, labels) in pairs(classes)
        foreach(labels) do label
            ratios[label] = classratios[class]
        end
    end
    return ratios
end
ungroupby(class, values::Dict{<:Any, T}) where {T} = ungroupby(T, class, values)

function ratio(mincount::Int, maxcount::Int, classcounts, classes)
    classratios = ratio(classcounts; lower=mincount, upper=maxcount)
    ungroupby(classes, classratios)
end
ratio(sampler::ClassRatios, counts) = ratio(countthresholds(sampler, counts)...)

function ratio(sampler::IntraClassRatios, counts)
    totalcounts = total(counts)
    mincount, maxcount, classcounts, classes = countthresholds(sampler, counts)
    classratios = ratio(classcounts; lower=mincount, upper=maxcount)
    ratios = ungroupby(Float64, classes, classratios)
    for (class, labels) in pairs(classes)
        for (selector, f) in pairs(sampler.intraclass)
            if !isnothing(match(selector, labels))
                updatedratios = f(class,
                                  classratios[class],
                                  Dict(label => totalcounts[label] for label in labels);
                                  lower=mincount,
                                  upper=maxcount)
                isnothing(updatedratios) || merge!(ratios, updatedratios)
            end
        end
    end
    return ratios
end

function prioritylabel(label; verbose=true)
    speciallabel = label
    selector = asselector(label)
    function priority_include(class, ratio, counts; lower=0, upper=typemax(Int))
        maxothers = 0
        preincluded = 0
        maxinclusions = 0
        ratios = Dict{eltype(keys(counts)), typeof(ratio)}()
        for (label, count) in pairs(counts)
            if match(selector, label)
                preincluded += round(Int, ratio * count)
                maxinclusions += count
            else
                maxothers += count
            end
        end
        inclusions = min(maxinclusions, upper)
        if 0 < preincluded < inclusions
            others = min(maxothers, upper) - inclusions
            if verbose
                @info "Explicit inclusions (initially selected => eventually selected):" class=class priority_tag=speciallabel with_priority_tag=(preincluded=>inclusions) without_priority_tag=(maxothers=>others)
            end
            priorityratio = inclusions / maxinclusions
            newratio = others / maxothers
            foreach(keys(counts)) do label
                ratios[label] = match(selector, label) ? priorityratio : newratio
            end
        end
        return ratios
    end
    return selector => priority_include
end

"""
    sample(f, loader, features=:spine)

Sample time segments and apply function `f` on the segments from each file.

Function `f` takes the following arguments:
* file index (`Int`),
* file object ([`PreloadedFile`](@ref Main.PlanarLarvae.Formats.PreloadedFile),
* couple of:
    * cumulated count of time segments prior to current file (`Int`),
    * expected count of time segments for current file (`Int`),
* list of time segments (`Vector{TimeSegment}`).

See also [`TimeSegment`](@ref).
"""
function sample(f, loader::DataLoader, features=:spine; kwargs...)
    ch = Channel() do ch
        state = nothing
        for (i, file) in enumerate(files(loader))
            state = presample(state, file, loader.window, loader.index)
            put!(ch, (i, file, state))
        end
    end
    Threads.foreach(ch) do (i, file, state)
        segments = sample(file, loader.window, loader.index, features; kwargs...)
        f(i, file, state, filter(!isnothing, segments))
        empty!(segments)
        unload!(file; gc=true)
    end
end

function sample(file::Formats.PreloadedFile, window, ix::LazyIndex, features; kwargs...)
    sample(ix.sampler, file, window, ix.targetcounts[file], features; kwargs...)
end

function sample(sampler, file, window, counts, features; verbose=false)
    # generate an index for the present file as a collection of pointers (pairs of track
    # number and time step index) for each behavioral class
    T = eltype(keys(counts))
    T′= Tuple{TrackID, Int, Symbol}
    run = Formats.getnativerepr(file)
    index = Dict{T, Vector{T′}}()
    for (trackid, track) in pairs(run)
        labels = getlabels′(track)
        ind = collect(indicator(window, track))
        for step in ind
            label = labels[step]
            class = match(sampler, label)
            isnothing(class) && continue
            labelindex = get!(index, label, T′[])
            push!(labelindex, (trackid, step, class))
        end
    end

    if verbose
        observedcounts = Dict(label=>length(pointers) for (label, pointers) in index)
        @info "In file: $(file.source)\nSample sizes (observed => selected):" [(label isa Vector ? Symbol(Symbol.(label)) : Symbol(label)) => (count => get(counts, label, 0)) for (label, count) in pairs(observedcounts)]...
    end

    rng = isnothing(sampler.rng) ? Random.default_rng() : sampler.rng

    # pick time segments at random to achieve the desired class counts
    for (label, count) in pairs(counts)
        label in keys(index) || continue
        pointers = shuffle(rng, index[label])
        # counts should not exceed the actual numbers
        index[label] = pointers[1:count]
    end
    index = sort(vcat(values(index)...))

    # load the data dependencies if any, skipping the data that are not requested
    # TODO: make `drop_record!(::JSONLabels)` manage the data dependencies as well,
    #       with e.g. spine/outline files removed from the list of dependencies if not
    #       needed, or `drop_record!` simply applied to a trx.mat file
    if features isa Symbol
        features = Set([features])
    end
    @assert :outline ∉ features
    if file isa Formats.JSONLabels
        Formats.getdependencies!(file)
        for file′ in file.dependencies
            if isempty(file′.timeseries)
                Formats.drop_outlines!(file′)
                :spine ∈ features || Formats.drop_spines!(file′)
                :tags  ∈ features || Formats.drop_record!(file′, :tags)
            end
            Formats.load!(file′)
        end
    else
        isempty(file.timeseries) || empty!(file.timeseries)
        capabilities = [cap[1] for cap in file.capabilities]
        for capability in (:spine, :outline, :tags)
            if capability in capabilities && capability ∉ features
                Formats.drop_record!(file, capability)
            end
        end
    end

    # normalize the timestamps
    Formats.normalize_timestamps(file)

    # return an iterator over all time segments
    map(index) do (trackid, step, class)
        segment(file, window, trackid, step, class)
    end
end

getlabels′(track::Track) = track[:labels]

function getlabels′(timeseries::LarvaBase.TimeSeries)
    [convert(Vector{String}, state[:tags]) for (_, state) in timeseries]
end

function presample(state, file::Formats.PreloadedFile, window, ix::LazyIndex)
    presample(ix.sampler, state, file, window, ix.targetcounts[file])
end
presample(_, ::Nothing, _, _, counts) = (0, sum(values(counts)))
presample(_, cumulatedcount, _, _, counts) = (sum(cumulatedcount), sum(values(counts)))

struct CapacitySampling <: RatioBasedSampling
    selectors
    maxcount::Integer
    rng
end

"""
    capacitysampling(maxcount; seed=nothing, rng=nothing)
    capacitysampling(selectors, maxcount; ...)

Important! Returns a [`LazyIndex`](@ref) object.

Sample each class up to a maximum count `maxcount`.
Class definition can be controlled with argument `selectors`.
"""
function capacitysampling(selectors, maxcount::Integer; seed=nothing, rng=nothing)
    if !isnothing(seed)
        if isnothing(rng)
            rng = Random.default_rng()
        end
        Random.seed!(rng, seed)
    end
    LazyIndex(CapacitySampling(asselectors(selectors), maxcount, rng))
end

function capacitysampling(maxcount::Integer; kwargs...)
    capacitysampling(nothing, maxcount; kwargs...)
end

function ratio(sampler::CapacitySampling, counts)
    maxcount = sampler.maxcount
    ratios = Dict(class => min(count, maxcount) / count for (class, count) in pairs(counts))
    return ratios
end

"""
    samplesize!(index, n)

Adjust target counts so that the total sample size is `n`.
This should be performed after [`buildindex`](@ref) and before [`sample`](@ref).

See also [`capacitysampling`](@ref) for per-class control of sample size.
"""
function samplesize!(index, sample_size)
    @assert !isnothing(index.targetcounts) "buildindex must be called before samplesize!"

    total_sample_size = length(index)
    sample_size < total_sample_size || return index

    ratio = sample_size / total_sample_size

    rng = isnothing(index.sampler.rng) ? Random.default_rng() : index.sampler.rng

    # apply `ratio` to the total counts first
    maxcounts = Dataloaders.total(index.maxcounts)
    targetcounts = Dataloaders.total(index.targetcounts)
    for (label, count) in pairs(targetcounts)
        targetcounts[label] = round(Int, count * ratio)
    end
    totalcount = sum(values(targetcounts))
    if totalcount < sample_size
        for label in shuffle(rng, collect(keys(targetcounts)))
            if targetcounts[label] < maxcounts[label]
                targetcounts[label] += 1
                totalcount += 1
                totalcount == sample_size && break
            end
        end
    elseif sample_size < totalcount
        for label in shuffle(rng, collect(keys(targetcounts)))
            if 0 < targetcounts[label]
                targetcounts[label] -= 1
                totalcount -= 1
                totalcount == sample_size && break
            end
        end
    end

    # apply `ratio` at the per-file level
    targetcountsperfile = copy(index.targetcounts)
    for counts in values(targetcountsperfile)
        for (label, count) in pairs(counts)
            counts[label] = round(Int, count * ratio)
        end
    end
    targetcounts′= Dataloaders.total(targetcountsperfile)
    for (label, targetcount) in pairs(targetcounts)
        count = targetcounts′[label]
        while count < targetcount
            file = rand(rng, collect(keys(targetcountsperfile)))
            counts = targetcountsperfile[file]
            if haskey(counts, label) && counts[label] < index.maxcounts[file][label]
                counts[label] += 1
                count += 1
            end
        end
        while targetcount < count
            file = rand(rng, collect(keys(targetcountsperfile)))
            counts = targetcountsperfile[file]
            if 0 < get(counts, label, 0)
                counts[label] -= 1
                count -= 1
            end
        end
    end

    index.targetcounts = targetcountsperfile
    return index
end

end
